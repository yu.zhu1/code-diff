package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import static org.junit.jupiter.api.Assertions.*;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;



    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-

        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        childEntity.setParentEntity(parentEntity);

        flushAndClear(em ->{
            childEntityRepository.save(childEntity);
        });

        ChildEntity fetchedChildEntity = childEntityRepository.findById(childEntity.getId()).get();
        assertNotNull(fetchedChildEntity.getParentEntity().getId());
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        childEntity.setParentEntity(parentEntity);

        flushAndClear(em ->{
            childEntityRepository.save(childEntity);
            childEntity.setParentEntity(null);
        });


        ChildEntity fetchedChildEntity = childEntityRepository.findById(childEntity.getId()).orElse(null);
        assertEquals(Long.valueOf(1),fetchedChildEntity.getId());

        ParentEntity fetchedParentEntity = parentEntityRepository.findById(parentEntity.getId()).get();
        assertEquals(Long.valueOf(1), fetchedParentEntity.getId());
        assertNull(fetchedChildEntity.getParentEntity());

        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-

        // --end-->
    }
}